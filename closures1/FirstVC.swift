//
//  FirstVC.swift
//  closures1
//
//  Created by Hamza on 2/12/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class FirstVC: UIViewController {

    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func btnHomePressed(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "SecondVC") as! SecondVC
        vc.completion = { dict in
            self.firstLabel.text = dict["name"] as? String
            self.secondLabel.text = dict["pass"] as? String
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    

}
