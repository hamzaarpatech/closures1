//
//  SecondVC.swift
//  closures1
//
//  Created by Hamza on 2/12/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class SecondVC: UIViewController {
    
    typealias completionHandler = ([String:Any]) -> Void // we pass dictionary thats why any
    var completion : completionHandler?
    @IBOutlet weak var firstTextField: UITextField!
    @IBOutlet weak var secondTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

    @IBAction func btnSavePressed(_ sender: UIButton) {
        guard let name = firstTextField.text else {return}
        guard let pass = secondTextField.text else {return}
        let dict = ["name" : name , "pass" : pass]
        guard let completionBlock = completion else {return}
        completionBlock(dict)
        self.navigationController?.popViewController(animated: true)
    }
    

}
